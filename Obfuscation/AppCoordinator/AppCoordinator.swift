import UIKit

class AppCoordinator: Coordinator {
    internal let parent: Coordinator! = nil
    private let window: UIWindow
    private var mainVC: MainVC?
    private var modalVCs: Array<UIViewController> = []

    private lazy var mainCoordinator: MainCoordinator = {
        MainCoordinator(
            parent: self,
            window: self.window,
            delegate: self)
    }()

    private lazy var paymentAccountCoordinator: PaymentAccountCoordinator = {
        PaymentAccountCoordinator(parent: self, delegate: self)
    }()

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        UIApplication.shared.isIdleTimerDisabled = true

        mainCoordinator.showMainController()
        mainVC = mainCoordinator.mainVC
        if let main = mainVC {
            modalVCs.append(main)
        }
    }

    func showViewController(
        _ vc: UIViewController,
        withPresentationStyle ps: PresentationStyle) {
        switch ps {
        case .push:
            if let cvc = modalVCs.last as? UINavigationController {
                cvc.pushViewController(vc, animated: true)
            } else {
                modalVCs.last?.navigationController?.pushViewController(vc, animated: true)
            }
        case .modal:
            let controller = vc is UINavigationController ? vc : UINavigationController(rootViewController: vc)
            controller.view.backgroundColor = UIColor.clear
            modalVCs.last?.present(controller, animated: true, completion: nil)
            modalVCs.append(controller)
        }
    }

    internal func dismissCurrentViewController() {
        let last = modalVCs.last
        if last == mainVC {
            _ = mainVC?.navigationController?.popToRootViewController(animated: true)
        } else {
            last?.view.endEditing(true)
            last?.dismiss(animated: true, completion: nil)
            modalVCs.removeLast()
        }
    }
}

extension AppCoordinator: MainVCDelegate {
    func showPaymentAccountView() {
        paymentAccountCoordinator.showPaymentAccount()
    }
}

extension AppCoordinator: PaymentAccountCoordinatorDelegate {
    func accountAdded(creditCardString: SecretString) {
        mainCoordinator.updateView(with: creditCardString)
    }
}
