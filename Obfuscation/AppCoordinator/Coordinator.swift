import UIKit

enum PresentationStyle {
    case push, modal
}

protocol Coordinator {
    func showViewController(_ vc: UIViewController,
                            withPresentationStyle ps: PresentationStyle)
    func dismissCurrentViewController()
    var parent: Coordinator! { get }
}

extension Coordinator {
    func showViewController(_ vc: UIViewController,
                            withPresentationStyle ps: PresentationStyle) {
        parent.showViewController(vc, withPresentationStyle: ps)
    }

    func dismissCurrentViewController() {
        parent.dismissCurrentViewController()
    }
}
