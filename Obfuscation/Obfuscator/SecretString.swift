//
//  SecretString.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 22/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

class SecretString {
    let obfuscator: ObfuscatorProtocol
    var value: String? {
        get {
            return obfuscator.decrypted(string: obfuscatedString)
        } set {
            guard let newStr = newValue else { return }

            obfuscatedString = obfuscator.encrypted(string: newStr) ?? ""
        }
    }

    private var obfuscatedString: String = ""

    // Should not be here. Created just to display obfuscated string for demo
    var demoObfuscatedString: String? {
        return obfuscatedString
    }

    init?(string: String) {
        obfuscator = Int(arc4random_uniform(2)) == 0 ? XORObfuscator() : AESObfuscator()

        guard let obfuscatedStr = obfuscator.encrypted(string: string) else { return nil }
        obfuscatedString = obfuscatedStr
    }

    init?(string: String, obfuscator: ObfuscatorProtocol) {
        self.obfuscator = obfuscator

        guard let obfuscatedStr = obfuscator.encrypted(string: string) else { return nil }
        obfuscatedString = obfuscatedStr
    }
}
