//
//  XORObfuscator.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

class XORObfuscator: ObfuscatorProtocol {
    private var key: [UInt8] = []

    init() {
        self.key = [UInt8](NSUUID().uuidString.utf8)
    }

    func encrypted(string: String) -> String? {
        guard let str = toBase64(string: string) else { return nil }
        return xorEncryption(string: str)
    }

    func decrypted(string: String) -> String? {
        guard let str = xorEncryption(string: string) else { return nil }
        return fromBase64(string: str)
    }

    // It would be more secure to return and store encrypted data in memory as [UInt8],
    // but I find it more convenitent to store it as string
    // as the same method can be used to encrypt and decrypt the string
    // To add more security I store secret key as [UInt8] array
    private func xorEncryption(string: String) -> String? {
        let text = [UInt8](string.utf8)
        let cipher = key
        let length = cipher.count

        let encrypted: [String] = text.enumerated().compactMap {
            let byte: UInt8 = $0.element ^ cipher[$0.offset % length]
            return String(bytes: [byte], encoding: String.Encoding.utf8)
        }

        if encrypted.count == text.count {
            return encrypted.joined()
        } else {
            return nil
        }
    }

    // One more way to encrypt the string, but the format is too recognizable, so I decided to use it together
    // with xor encryption just for demonstration
    private func toBase64(string: String) -> String? {
        guard let data = string.data(using: String.Encoding.utf8) else { return nil }

        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }

    private func fromBase64(string: String) -> String? {
        guard let data = Data(base64Encoded: string, options: Data.Base64DecodingOptions(rawValue: 0)) else { return nil }

        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
}
