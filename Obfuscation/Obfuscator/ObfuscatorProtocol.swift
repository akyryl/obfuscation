//
//  ObfuscatorProtocol.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

protocol ObfuscatorProtocol {
    func encrypted(string: String) -> String?
    func decrypted(string: String) -> String?
}
