//
//  AESObfuscator.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

class AESObfuscator: ObfuscatorProtocol {
    private var key: [UInt8] = []

    init() {
        self.key = [UInt8](NSUUID().uuidString.utf8)
    }

    func encrypted(string: String) -> String? {
        guard
            let keyData = String(bytes: key, encoding: String.Encoding.utf8)?.data(using: String.Encoding.utf8),
            let data = string.data(using: String.Encoding.utf8),
            let stringData = NSMutableData(length: Int((data.count)) + kCCBlockSizeAES128) else { return nil }

        var encryptedBytesCount: size_t = 0
        let status = CCCrypt(
            UInt32(kCCEncrypt),
            UInt32(kCCAlgorithmAES128),
            UInt32(kCCOptionPKCS7Padding | kCCModeCBC),
            (keyData as NSData).bytes,
            size_t(kCCKeySizeAES128),
            nil,
            (data as NSData).bytes,
            data.count,
            stringData.mutableBytes,
            stringData.length,
            &encryptedBytesCount)

        if status == kCCSuccess {
            stringData.length = Int(encryptedBytesCount)
            return stringData.base64EncodedString(options: .lineLength64Characters)
        } else {
            return nil
        }
    }
    func decrypted(string: String) -> String? {
        guard let keyData = String(bytes: key, encoding: String.Encoding.utf8)?.data(using: String.Encoding.utf8),
            let data = NSData(base64Encoded: string, options: .ignoreUnknownCharacters),
            let stringData = NSMutableData(length: Int((data.length)) + kCCBlockSizeAES128) else { return nil }

        var decryptedBytesCount: size_t = 0
        let status = CCCrypt(
            UInt32(kCCDecrypt),
            UInt32(kCCAlgorithmAES128),
            UInt32(kCCOptionPKCS7Padding | kCCModeCBC),
            (keyData as NSData).bytes,
            size_t(kCCKeySizeAES128),
            nil,
            data.bytes,
            data.length,
            stringData.mutableBytes,
            stringData.length,
            &decryptedBytesCount)

        if UInt32(status) == UInt32(kCCSuccess) {
            stringData.length = Int(decryptedBytesCount)
            return String(data: stringData as Data, encoding:String.Encoding.utf8)
        } else {
            return nil
        }
    }
}
