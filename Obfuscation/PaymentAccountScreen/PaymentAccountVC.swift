//
//  PaymentAccountVC.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class PaymentAccountVC: UIViewController {
    var input: PaymentAccountInput
    let output: PaymentAccountOutput

    @IBOutlet weak var creditCardNumberTextField: UITextField!

    init(input: PaymentAccountInput, output: PaymentAccountOutput) {
        self.input = input
        self.output = output

        super.init(nibName: "PaymentAccountVC", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    private func setupViews() {
        creditCardNumberTextField.delegate = self
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        title = input.navigationBarTitle
        addCancelBarButton(action: #selector(cancelButtonTapped))
        addSaveBarButton(action: #selector(saveButtonTapped))
    }

    @IBAction func creditNumberTextChanged(_ sender: Any) {
        formatEnteredText()
    }

    @objc
    private func cancelButtonTapped() {
        output.onCancel()
    }

    @objc
    private func saveButtonTapped() {
        guard let text = creditCardNumberTextField.text else {
            return
        }

        if let cardString = SecretString(string: text) {
            output.onSave(creditCardString: cardString)
        }
    }

    private func formatEnteredText() {
        var cursorPosition: Int?
        if let selectedRange = creditCardNumberTextField.selectedTextRange {
            cursorPosition = creditCardNumberTextField.offset(from: creditCardNumberTextField.beginningOfDocument, to: selectedRange.start)
        }
        creditCardNumberTextField.attributedText = formatEnteredText(from: self.creditCardNumberTextField.text ?? "")
        if let previousPosition = cursorPosition,
            let newPosition = creditCardNumberTextField.position(from: creditCardNumberTextField.beginningOfDocument, offset: previousPosition) {
            creditCardNumberTextField.selectedTextRange = creditCardNumberTextField.textRange(from: newPosition, to: newPosition)
        }
    }

    private func formatEnteredText(from string: String) -> NSAttributedString {
        let groupedSectionSize: Int = 4
        let kern: CGFloat = 8.0

        let result = NSMutableAttributedString(string: string.uppercased().replacingOccurrences(of: " ", with: ""))
        var idx = groupedSectionSize
        while idx < result.length {
            result.setAttributes([.kern: kern], range: NSRange(location: idx - 1, length: 1))
            idx += groupedSectionSize
        }
        return result
    }
}

extension PaymentAccountVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveButtonTapped()
        return true
    }
}
