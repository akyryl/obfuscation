//
//  PaymentAccountCoordinator.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

protocol PaymentAccountCoordinatorDelegate {
    func accountAdded(creditCardString: SecretString)
}

class PaymentAccountCoordinator {
    let parent: Coordinator!
    var delegate: PaymentAccountCoordinatorDelegate

    init(
        parent: Coordinator,
        delegate: PaymentAccountCoordinatorDelegate) {
        self.parent = parent
        self.delegate = delegate
    }

    func showPaymentAccount() {
        let input: PaymentAccountInput = PaymentAccountVM()
        let paymentAccountVC = PaymentAccountVC(input: input, output: self)
        parent.showViewController(paymentAccountVC, withPresentationStyle: .modal)
    }
}

extension PaymentAccountCoordinator: PaymentAccountOutput {
    func onCancel() {
        parent.dismissCurrentViewController()
    }

    func onSave(creditCardString: SecretString) {
        parent.dismissCurrentViewController()

        delegate.accountAdded(creditCardString: creditCardString)
    }
}
