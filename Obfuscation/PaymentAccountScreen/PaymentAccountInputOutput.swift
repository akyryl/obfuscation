//
//  PaymentAccountInputOutput.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

protocol PaymentAccountInput {
    var navigationBarTitle: String { get }
}

protocol PaymentAccountOutput {
    func onCancel()
    func onSave(creditCardString: SecretString)
}

class PaymentAccountVM: PaymentAccountInput {
    var navigationBarTitle: String = "Add Payment Account"
}
