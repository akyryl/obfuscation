//
//  UIViewControllerExtensions.swift
//  Obfuscation
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

extension UIViewController {
    func addCancelBarButton(action: Selector) {
        let button = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: action)
        navigationItem.rightBarButtonItem = button
    }

    func addSaveBarButton(action: Selector) {
        let button = UIBarButtonItem(title: "Save", style: .plain, target: self, action: action)
        navigationItem.leftBarButtonItem = button
    }
}
