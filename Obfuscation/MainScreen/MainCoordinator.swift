import UIKit

protocol MainVCDelegate {
    func showPaymentAccountView()
}

class MainCoordinator: Coordinator {
    let parent: Coordinator!

    private let window: UIWindow
    var mainVC: MainVC?
    let delegate: MainVCDelegate

    init(parent: Coordinator,
         window: UIWindow,
         delegate: MainVCDelegate) {
        self.parent = parent
        self.window = window
        self.delegate = delegate
    }

    func showMainController() {
        let input = MainVM()
        mainVC = MainVC(input: input, output: self)

        window.rootViewController = mainVC
        window.makeKeyAndVisible()
    }

    func updateView(with creditCardString: SecretString) {
        mainVC?.input = MainVM(creditCardString: creditCardString)
    }
}

extension MainCoordinator: MainOutput {
    func onShowPaymentAccount() {
        delegate.showPaymentAccountView()
    }
}
