//
//  MainInputOutput.swift
//  CustomAlert
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

protocol MainInput {
    var alertButtonTitle: String { get }
    var creditCardString: SecretString? { get }
}

protocol MainOutput {
    func onShowPaymentAccount()
}

class MainVM: MainInput {
    let alertButtonTitle: String = "Add Payment Account"

    var creditCardString: SecretString?

    init(creditCardString: SecretString? = nil) {
        self.creditCardString = creditCardString
    }
}
