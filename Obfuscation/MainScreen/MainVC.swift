import UIKit

class MainVC: UIViewController {
    var input: MainInput {
        didSet {
            updateControls()
        }
    }
    fileprivate let output: MainOutput

    @IBOutlet weak var paymentMethodButton: UIButton!
    @IBOutlet weak var bankAccountLabel: UILabel!
    @IBOutlet weak var encryptedAccountLabel: UILabel!

    init(input: MainInput, output: MainOutput) {
        self.input = input
        self.output = output

        super.init(nibName: "MainVC", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupControls()
    }

    private func setupControls() {
        updateControls()
    }

    private func updateControls() {
        paymentMethodButton.setTitle(input.alertButtonTitle, for: .normal)

        if let cardString = input.creditCardString {
            bankAccountLabel.text = cardString.value

            // Just for demo
            encryptedAccountLabel.text = cardString.demoObfuscatedString
        }
    }

    @IBAction func paymentMethodButtonTapped() {
        output.onShowPaymentAccount()
    }
}
