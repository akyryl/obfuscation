//
//  MockObfuscator.swift
//  ObfuscationTests
//
//  Created by Anatolii Kyryliuk on 22/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import Foundation

class MockObfuscator: ObfuscatorProtocol {
    var encryptedCallsCount: Int = 0
    var decryptedCallsCount: Int = 0

    func encrypted(string: String) -> String? {
        encryptedCallsCount += 1
        return ""
    }

    func decrypted(string: String) -> String? {
        decryptedCallsCount += 1
        return ""
    }
}
