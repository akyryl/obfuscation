//
//  ObfuscationTests.swift
//  ObfuscationTests
//
//  Created by Anatolii Kyryliuk on 16/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import XCTest
@testable import Obfuscation

class ObfuscationTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSecretString() {
        let testString = "Secrect String"
        let mockObfuscator: MockObfuscator = MockObfuscator()
        guard let secrectString = SecretString(string: testString, obfuscator: mockObfuscator) else {
            XCTAssert(false, "Unable to create SecretString object with mock obfuscator")
            return
        }
        _ = secrectString.value
        XCTAssert(mockObfuscator.encryptedCallsCount == 1)
        XCTAssert(mockObfuscator.decryptedCallsCount == 1)

        secrectString.value = "New Secrect String"
        _ = secrectString.value
        XCTAssert(mockObfuscator.encryptedCallsCount == 2)
        XCTAssert(mockObfuscator.decryptedCallsCount == 2)
    }

    func testSecretStringBehaviour() {
        let testString = "1234 5678 9023 5432"
        let secrectString = SecretString(string: testString)
        XCTAssert(testString == secrectString?.value)
    }
    
    func testStaticStrings() {
        _ = obfuscateTest(secretString: "my secrect string 1", obfuscator: XORObfuscator())
        _ = obfuscateTest(secretString: "1234 5678 9012 3456", obfuscator: XORObfuscator())
        _ = obfuscateTest(secretString: "!@#$%^&*()<>?\"\'`~", obfuscator: XORObfuscator())
        _ = obfuscateTest(secretString: "MY-SECRET-STRING-1", obfuscator: XORObfuscator())
    }

    func testEqualStrings() {
        let secretString = "1111111111111111"
        let obfuscated1 = obfuscateTest(secretString: secretString, obfuscator: XORObfuscator())
        let obfuscated2 = obfuscateTest(secretString: secretString, obfuscator: XORObfuscator())
        let obfuscated3 = obfuscateTest(secretString: secretString, obfuscator: AESObfuscator())
        let obfuscated4 = obfuscateTest(secretString: secretString, obfuscator: AESObfuscator())

        XCTAssert(obfuscated1 != obfuscated2, "Obfuscator generates equal strings")
        XCTAssert(obfuscated3 != obfuscated4, "Obfuscator generates equal strings")
        XCTAssert(obfuscated1 != obfuscated3, "Different obfuscators generate equal strings")
    }

    func testXORObfuscatorPerformance() {
        let original = NSUUID().uuidString
        self.measure {
            for _ in 0...1000 {
                _ = obfuscateTest(secretString: original, obfuscator: XORObfuscator())
            }
        }
    }

    func testAESObfuscatorPerformance() {
        let original = NSUUID().uuidString
        self.measure {
            for _ in 0...1000 {
                _ = obfuscateTest(secretString: original, obfuscator: AESObfuscator())
            }
        }
    }

    func obfuscateTest(secretString: String, obfuscator: ObfuscatorProtocol) -> String? {
        if let obfuscatedString = obfuscator.encrypted(string: secretString) {
            if let original = obfuscator.decrypted(string: obfuscatedString) {
                XCTAssert(secretString == original)

                // print("-- ORIGINAL: \(original)")
                // print("-- OBFUSCATED: \(obfuscatedString)")
                return obfuscatedString
            } else {
                XCTAssert(false, "Failed to decrypt string: \(secretString)")
            }
        } else {
            XCTAssert(false, "Failed to encrypt string: \(secretString)")
        }
        return nil
    }
}
